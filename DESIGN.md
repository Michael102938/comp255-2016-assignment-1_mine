
This file should describe your design choices and motivates the choices.

The program contain one class ProcessManager, it contains functions as follow:

A constructor public ProcessManager(String executable, String[] args) 
	+get the input from user then assign it to local variables.
	+initialize the processBuilder variable.
	
A method to begin process public void spawn()
	+start the process that being defined in constructor above.
	
A method that start the process and collect the output: public String spawnAndCollect():
	+collect the output from process that being run and return a string of all the output.
	+Using BufferReader to connect the input stream of process then assign it to StringBuilder, therefore builder.toString() return the output of the process.
	
The function public String spawnAndCollectWithTimeOut(int timeLimit)
	+set a time limit to the function spawnAndCollect, if the process run longer than the limit time, the function will throw a TimeoutException.
	+using process.waitfor() method to count the time that process run.
	+if the process run longer than the time limit but there are output it will still return the output but at same time output a error message.
	+if the process run longer than time limit but no output from process then the process will end with TimeoutException being throws and an error message will be print out.

The send(String s) method:
	+send a string to the running process, if the process is not alive it start a new process.
	-Junit testing for the send method did not use assertTrue or assertEquals, but instead I used system.out.println to output the result of the process.
	-In this way, I can see clearly what the process and the methods is doing.
	-Junit of send is include in junit of expect method
	
The expect(Pattern s, int timeout) method:
	+it will use the pattern in parameter and search for it from beginning of the process output till it meet the pattern in the output then it will return all the string above that pattern.
	-Junit for expect() method, I used system.out.println to output the result, so that I can test  what the method actually doing and what will happen if I change the script file or when I change the pattern parameter.
	-Junit method expectForTimeout() will test the expect() method for when the process take longer than the time limit, in the junit I catch the timeoutException from the expect() method and output the message of the exception using e.getmessage().
	-The result of testing is the method do what I intended it to do.

Method to terminate process Public void Destroy()
	+force the process to close.