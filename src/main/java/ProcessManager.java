import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

/**
 * Create a running process and manage interaction with it
 */
public class ProcessManager {

    /**
     * The strings for which the analyser is built
     */
    String      program;
    String[]    arguments;
    ProcessBuilder pb;
    Process p;
    // FIXME if you need to add more variables

    /**
     * Make system running process with
     *
     *  @param  executable    The program to run
     *  @param  args          The arguments of the program
     *  
     *  initializing the processes
     */
    public ProcessManager(String executable, String[] args) {
        program = executable;
        arguments = args;
        pb=new ProcessBuilder(program,arguments[0]);
    }
    
    public ProcessManager(String path){
    	pb=new ProcessBuilder(path);
    }

    /**
     * Spawn a process
     */
     public void spawn() {
         // begin the process
    	 try {
			p= pb.start();
		} catch (IOException e) {
			// error message
			System.out.println("Error: IOException!");
		}
     }

    	
     /**
     * Send a string to the process 
     * @throws IOException 
     */
     public boolean send(String s) throws IOException { // FIXME and write the code return false;
    	 BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
    	 try{
    	 writer.write(s);
    	 }catch(IOException e){
    		 return false;
    	 }
    	 //writer.flush();
    	 try{
    	 writer.close();
    	 }catch(IOException e){
    		 return false;
    	 }
    	 return true;
     }
    
     /**
     * collect the result
     * up to a ‘prompt‘ or throw an
     * exception if the prompt is not seen before the timeout *
     * @param timeout The timeout in milliseconds
     * @param prompt The expected prompt
     * @throws TimeoutException 
     */
  	 @SuppressWarnings("resource")
	public String expect(Pattern prompt, int timeout) throws TimeoutException {
  		//once the prompt is meet, checker will be set true
  		 Boolean checker=false;
  		 Boolean TMchecker=false;
  		 String result="";
  		 StringBuilder builder = new StringBuilder();
		 String line = null;
  		 //collect the output, throw exception if it take too long
  		 long timeLimit=System.currentTimeMillis()+timeout;
  		 if(!p.isAlive())
  			 this.spawn();
  		 BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
  		 if(p.isAlive()){
  			 //line will get the string from current reader
  			 //then builder will add that line
  			 //after that builder will add "go to next line"
  			 try {
  				 //while within time limit we run the while loop
  				 while ( (System.currentTimeMillis()<=timeLimit)) {
  					 if((line = reader.readLine()) != null){
  						 Scanner scanner = new Scanner(line);			 
  						 if(!scanner.hasNext(prompt)){				 
  							 builder.append(line);					 
  							 builder.append(System.getProperty("line.separator"));				 
  						 }
  						 else {		
  							 //found the expected prompt so we break out of loop
  							 checker=true;						 
  							 break;						
  						 }
  					 }else{
  						 //finish reading from reader so we break out of loop
  						 break;
  					 }
  				 }
  			 } catch (IOException e) {
  				 e.printStackTrace();
  			 }
  			if(System.currentTimeMillis()>timeLimit){
					TMchecker=true;
					this.destroy();
					throw new TimeoutException("No prompt that we expected within time limit");
				}
  		 }
  		 //no prompt in all of the output
  		 if (checker==false){
 			 throw new TimeoutException("No prompt that we expected in the output");
 		 }
  		 //assign builder to result and return the string result
  		 result =""+ builder.toString();
  		 return result; 
  	 }
     
     
    /**
     * Spawn a process and collect the results
     */
     public String spawnAndCollect() {
         //    builder act as an array to collect all the output
    	 // reader will read from input stream that connect to the output
    	 String result="";
    	 this.spawn();
    	 StringBuilder builder = new StringBuilder();
		 String line = null;
		 BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		 
    	 if(p.isAlive()){
    		 //line will get the string from current reader
    		 //then builder will add that line
    		 //after that builder will add "go to next line"
    		 try {
    			 while ( (line = reader.readLine()) != null) {
    				 builder.append(line);
    				 builder.append(System.getProperty("line.separator"));
    			 }
    		 } catch (IOException e) {
    			 e.printStackTrace();
    		 }
    	 
    		
    	 }
    	//assign all of builder string to a string variable.
		 result = builder.toString();
    	 return result;
     }

     /**
      * Spawn a process and collect the results or throw an
      * exception if no answer before the timout
      *
      * @param  timeout     The timeout in milliseconds
     * @throws TimeoutException 
     *  
      */
      public String spawnAndCollectWithTimeout(int timeout) throws TimeoutException {
          //    the function will stop if no output is returned
    	  //it will output an exception if it stop without normal output
    	  String result = "";
    	  
    	  //declare timeLimit to check the if the run time has reached timeout or not
    	  long timeLimit = System.currentTimeMillis()+timeout;
    	  
    	  //this checker variable is used to check if the process excesses the time limit
    	  //return true if the process terminate before time limit
    	  //return false if the process keep running after time limit
    	  boolean checker=false;
    	  this.spawn();
    	  
    	  boolean repeat=true;
    	  //if while loop stop if the output within the timeLimit
    	  while(repeat==true){
    		  // checking timeout with boolean value
    			  try{
    				  checker =p.waitFor(timeout, TimeUnit.MILLISECONDS);
    			  }catch(InterruptedException e1){
    				  e1.printStackTrace();
    			  }
    		  
    		  //declare reader to read from output stream of the process
    		  BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
    		  //declare stringBuilder to store all the outputs of the process
    		  StringBuilder builder = new StringBuilder();
    		  String line = null;
    		  //line will get the string from current reader
    		  //then builder will add that line
    		  //after that builder will add "go to next line"
    		  try {
    			  while ( (line = reader.readLine()) != null) {
    				  builder.append(line);
    				  builder.append(System.getProperty("line.separator"));
    			  }
    			  
    		  } catch (IOException e) {
    			  e.printStackTrace();
    		  }
    		  //assign all of builder string to a string variable.
    		  result =""+ builder.toString();
    	  
    	  
    		  //handle timeout exception if the process run over time limit
    		  if(System.currentTimeMillis()>=timeLimit){
    			if((System.currentTimeMillis()-timeLimit)<2000){
    			  if(builder.toString().isEmpty() && result==""){
    				  repeat=false;
    				  //handle exception if no output before or after timeout
    				  if(checker==false){
    					  this.destroy();
    					  throw new TimeoutException("Error timeout, no output before time limit.");
    				  } else 
    					  //if process run within time limit it return result without exception
    					  if(checker==true){
    						  return result;
    					  }
    			  }
    			  else{
    				  //if process still running then repeat the loop to get the new output if there is any
    				  if(p.isAlive() )
    					  repeat=true;
    				  else {
    					  //handle exception timeout when there is output after timeout
    					  if(checker==false){
        					  this.destroy();
        					  throw new TimeoutException("Error: timeout! But there is output sometime after time limit.");
        					  
        				  }
    					  return result;
    				  }
    		  		}
    			}else{
    				repeat=false;
    				p.destroyForcibly();
					throw new TimeoutException("Error timeout, no output before time limit.");
    			}
    	  		} else {
    	  			//process finish within time limit
    	  			repeat=false;
    	  		}
    	  }
    	  return result;
      }

     /**
      * Kill the process
      */
      public void destroy() {
         // force close process
    	 p.destroy();
      }
      
      //access process in this class from outside
      public Process getProcess(){
      	return p;
      }
}
