
import org.junit.Test;

import sun.misc.IOUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.*;
import java.nio.charset.StandardCharsets;

/**
 * Test suite for Longest Common Subsequences
 */
public class ProcessManagerTests {

    /**
     * Test for spawning a process
     */
    @Test
    public void testSpawn() {

        //  FIXME
        // test program to spawn process
        String prog = "ps";
        String[] args = { "aux" };

        //  create a process manager to interact with `ps`
        ProcessManager p = new ProcessManager(prog, args);
        //  spawn a process that runs `ps aux` and collect the result
        p.spawn();
        int test=-1;
		try {
			test = p.getProcess().waitFor();
		} catch (InterruptedException e) {
			// error message
			System.out.println("process being interupted!");
		}
		//output 0 from process.waitfor() mean the process properly close
        assertEquals(0, test);
    }
    
    /**
     * Test for spawning and collect output
     */
    @Test
    public void testSpawnAndCollect() {

        //  FIXME
        // test program to spawn and collect output
        String prog = "ps";
        String[] args = { "ax" };

        //  create a process manager to interact with `ps`
        ProcessManager p = new ProcessManager(prog, args);
        //spawn and collect result
        String res = null;
        res = p.spawnAndCollect();
        //success capture the output to string mean string will not be null
        assertTrue(res!=null);
    }
    
    /**
     * Test for Send and expect method
     * @throws IOException 
     * @throws TimeoutException 
     */
    @Test
    public void expect() throws IOException, TimeoutException {
        //  test program to run script
    	String workingDir = System.getProperty("user.dir");
        String scriptloc= workingDir + "/ExpectTester.sh";
        //  create a process manager to interact with `ps`
        ProcessManager p = new ProcessManager(scriptloc);
        String res="empty";
        //spawn and collect result
        p.spawn();
        res=p.expect(Pattern.compile(".ow"), 2000);
        String inputToSend[]={"I am happy","23","4/11/2016"};
        p.send(inputToSend[0]);
        
        
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getProcess().getInputStream()));
        

        System.out.println("");
        System.out.println("Here is the testing of the script:\n");
        // read the output from the command
        String s = null;
        while ((s = stdInput.readLine()) != null) {
        	System.out.println(s);
        }
        System.out.println("");
        System.out.println("The String from the Expect method: ");
        System.out.println(res);
        //success capture the output to string mean string will not be null
        assertTrue(s==null);
    }
    
    /**
     * Test for Send and expect method 
     * we testing timeout in this test
     * @throws IOException 
     * @throws TimeoutException 
     */
    @Test
    public void expectForTimout() throws IOException, TimeoutException {
    	//sleep for 5 seconds but time limit is 2 seconds
    	String prog = "sleep";
        String[] args = { "5" };
        String res=null;
        ProcessManager p = new ProcessManager(prog,args);
        //spawn and collect result
        p.spawn();
        System.out.println("");
        System.out.println("");
        System.out.println("This is the test for expect method with timeout: ");
        try{
        res=p.expect(Pattern.compile(".ow"), 2000);
        }catch(TimeoutException e){
        	System.out.println(e.getMessage());
        }
        //res equal null because the method stopped
        assertTrue(res==null);
    }
    
    /**
     * Test for spawning and collect output
     *
     */
    @Test
    public void testSpawnAndCollectWithTimeout()  {

        //  FIXME
        // test program to spawn and collect output with timeout
        String prog = "ps";
        String[] args = { "ax" };

        //  create a process manager to interact with command
        ProcessManager p = new ProcessManager(prog, args);
        //spawn and collect result
        String res = null;
        //check if the timeout will output an error for when time limit is reached
        try{
        res = p.spawnAndCollectWithTimeout(200);
        }catch(TimeoutException e){
        	//success capture the exception mean the code bellow will run
        	System.out.println("Process 1: "+e.getMessage());
        }
        //success capture the output to string mean string will not be null
        assertTrue(res!=null);
        
        res = null;
        try{
            res = p.spawnAndCollectWithTimeout(-200);
            }catch(TimeoutException e){
            	//success capture the exception mean the code bellow will run
            	System.out.println("Process 2: "+e.getMessage());
            	assertTrue(res==null);
            }
        
        prog = "sleep";
        String[] args1 = { "3" };

        //  create a process manager to interact with command
        ProcessManager p1 = new ProcessManager(prog, args1);
        //spawn and collect result
        String res1 = null;
        //check if the timeout will output an error for when time limit is reached
        try{
        	res1 = p1.spawnAndCollectWithTimeout(4000);
            }catch(TimeoutException e){
                //success capture the exception mean the code bellow will run
            	//it will not run if the timeout is longer than the wait time
            	System.out.println("Process 3: "+e.getMessage());
            }
        //success capture the output to string mean string will not be null
        assertTrue(res1!=null);
        //sleep commands will return an empty output
        assertEquals(res1,"");
        
        res1 = null;
        //check if the timeout will output an error for when time limit is reached
        try{
        	res1 = p1.spawnAndCollectWithTimeout(100);
            }catch(TimeoutException e){
            	//success capture the exception mean the code bellow will run
            	System.out.println("Process 4: "+e.getMessage());
            	//success capture the output to string mean string will not be null
            	assertTrue(res1==null);
                assertEquals(res1,null);
            }
        
        //check if the process will not output anything for very long time
        //or the process will not output anything at all
        //or you could say the process in a forever loop
        prog = "sleep";
        String[] args2 = { "3" };

        //  create a process manager to interact with command
        ProcessManager p2 = new ProcessManager(prog, args2);
        //spawn and collect result
        String res2 = null;
        //check if the timeout will output an error for when time limit is reached
        try{
        	res2 = p2.spawnAndCollectWithTimeout(100);
            }catch(TimeoutException e){
                //success capture the exception mean the code bellow will run
            	//it will not run if the user set timeout is longer than the
            	//command wait time
            	System.out.println("Process 5: "+e.getMessage());
            	//success capture the output to string mean string will not be null
            	//in case the exception is throw the method return nothing
            	// it mean the string variable keep the default value
            	//which is null value
                assertTrue(res2==null);
                //sleep commands will return an empty output
                assertEquals(res2,null);
            }
    }
    
    /**
     * Test for destroy a process
     */
    @Test
    public void testDestroy() {

        //  FIXME
        //  test process destroy
    	//these variables is the command input
        String prog = "ps";
        String[] args = { "aux" };

        //  create a process manager to interact with `ps`
        ProcessManager p = new ProcessManager(prog, args);
        //  spawn a process that runs `ps aux` and collect the result
        p.spawn();
        //force close
        p.destroy();
        int test=-1;
		try {
			test = p.getProcess().waitFor();
		} catch (InterruptedException e) {
			// error message
			System.out.println("process being interupted!");
		}
		//the output for p.waitfor()==0 mean it run and close properly
		//the output from process.waitfor() != 0 mean it did not terminate normally (forced close)
        assertTrue(test!=0);
    }

}
