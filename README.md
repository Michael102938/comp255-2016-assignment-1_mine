
This file should provide set up, requirements, and usage examples.

Set up
======

You need a recent JDK on your machine.

Recent versions of junit and harmcrest and provided in  src/test/java/lib/

Moreover, the CLASSPATH should be set as follows:

~~~
> export CLASSPATH="$CLASSPATH:src/test/java/lib/*:src/main/java/:src/test/java/"
~~~

Compiling
=========

~~~
> javac src/main/java/ProcessManager.java 
> javac src/test/java/ProcessManagerTests.java
> javac src/test/java/TestRunner.java
~~~


Running the tests
=================

~~~
> java TestRunner
~~~

The shell script "runall.sh" wil set the classpath, compile and run the tests.

You can also run the Junit test directly from JDK like eclipse

What the program do
===================

The program take command line as variables and start the process accordingly. (e.g: ps aux)